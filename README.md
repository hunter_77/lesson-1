## Task 1

Write a program that takes a message from an input and makes every first letter of its words uppercase and shows the result on the document.

Input:
![image.png](./image.png)

Output:
![image-1.png](./image-1.png)

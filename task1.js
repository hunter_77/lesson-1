var sentence = 'Alexandr Sergeevich Pushkin was one of the greatest poets of the world';

var capitalizeString = (str) => str[0].toUpperCase() + 
str.slice(1).toLowerCase();

var capitalizeWords = (str) => str.split(' ').map
(capitalizeString).join(' ');

console.log(capitalizeWords(sentence));